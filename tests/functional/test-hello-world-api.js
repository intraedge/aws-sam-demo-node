const request = require('supertest');

require('dotenv').config();

const { API_BASE } = process.env;

describe('GET /user', () => {
  it('responds with json', async () => request(API_BASE)
    .get('/hello')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200));
});
